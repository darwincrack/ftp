// JavaScript Document


$(document).ready(function(){
	//detalles csv
   $('#detalles_cvs').dataTable( { //CONVERTIMOS NUESTRO LISTADO DE LA FORMA DEL JQUERY.DATATABLES- PASAMOS EL ID DE LA TABLA
   "language": {
                "url": "../../js/datatable/1.10.4/Spanish.json"
               },
	"lengthMenu": [[100, 200, 500, -1], [100, 200, 500, "All"]],
	'iDisplayLength': 100,
	"bSort": false,
	
	"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '.')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
			
			//montoBsSaren, Total over all pages
			  TotalmontoBsSaren = api
                .column( 12 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } );
				
			//montoBsSaren, Total over this page
            PagemontoBsSaren = api
                .column( 12, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			
			
			//montoBsSat, Total over all pages
			  TotalmontoBsSat = api
                .column( 14 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } );
				
			 // montoBsSat, Total over this page
            PagemontoBsSat = api
                .column( 14, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			
			
            // total tramite, Total over all pages
            total_tramite = api
                .column( 15 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } );
 
            // total tramtie, Total over this page
            pageTotalTramite = api
                .column( 15, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
			//
			//montoBsSaren
			sumTotalPagemontoBsSaren= parseFloat(PagemontoBsSaren).toFixed(2);
			sumTotalPagemontoBsSaren=sumTotalPagemontoBsSaren.replace('.',',');
			sumTotalmontoBsSaren= parseFloat(TotalmontoBsSaren).toFixed(2);
			sumTotalmontoBsSaren=sumTotalmontoBsSaren.replace('.',',');
			
			//montoBsSat
			sumTotalPagemontoBsSat= parseFloat(PagemontoBsSat).toFixed(2);
			sumTotalPagemontoBsSat=sumTotalPagemontoBsSat.replace('.',',');
			sumTotalmontoBsSat= parseFloat(TotalmontoBsSat).toFixed(2);
			sumTotalmontoBsSat=sumTotalmontoBsSat.replace('.',',');
			
			
			//total tramite
			sumTotalPageTramite= parseFloat(pageTotalTramite).toFixed(2);
			sumTotalPageTramite=sumTotalPageTramite.replace('.',',');
			sumTotalTramite= parseFloat(total_tramite).toFixed(2);
			sumTotalTramite=sumTotalTramite.replace('.',',');
			
			
        /*    $( api.column( 15 ).footer() ).html(
                'Total Pag. Bs. '+sumTotalPageTramite +' (Total: Bs. '+ sumTotalTramite +')'
            );*/
			$("#montoBsSaren").html('Total Pag. Bs. '+sumTotalPagemontoBsSaren +'<br> (Total: Bs. '+ sumTotalmontoBsSaren +')');
			$("#montoBsSat").html('Total Pag. Bs. '+sumTotalPagemontoBsSat +'<br> (Total: Bs. '+ sumTotalmontoBsSat +')');
			$("#total_tramite").html('Total Pag. Bs. '+sumTotalPageTramite +'<br> (Total: Bs. '+ sumTotalTramite +')');
        }
		
    } );
	
	
	
	
	
	//lista de csv
   $('#lista_csv').dataTable( { //CONVERTIMOS NUESTRO LISTADO DE LA FORMA DEL JQUERY.DATATABLES- PASAMOS EL ID DE LA TABLA
   "language": {
                "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
               },
	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	"order": [[ 0, "desc" ]]
	
    } );

		
})


