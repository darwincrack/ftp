// JavaScript Document

     $(document).ready(function(){ //inicio jquery

        $(document).on('click', 'a.export_bd',function (){
			var datos='';
			var oID = $(this).attr("id");
			$.ajax({url: "ftp/overwrite_csv_bd",
			type:"POST",
			data: { name_csv		: oID,
				    validar_exist	: 1 
				  },
			async:false,
			success: function(result)
			{
      			datos=result;
				
    		}
		});
			

			//no existe en la bd
			if (datos==0)
			{
				
				$.post("ftp/overwrite_csv_bd",
           		{
            		name_csv 	: oID,
					ingresar_new:	1,
					overwrite	:	0
           		},
           		function(data) 
		   		{
					alert(data);
				});
			}
			//existe en la bd, sobreescritura
			else if (datos==1)
			{
				 var txt;
				 var r = confirm("El Archivo "+oID+"\n Existe en la Base de Datos, ¿Desea Reemplazarlo?");
				if (r == true) {
					
					$.post("ftp/overwrite_csv_bd",
           			{
            		name_csv 	: oID,
					ingresar_new:	0,
					overwrite	:	1
           			},
           			function(data) 
		   			{
						alert(data);
					});

				} 
			}
			
			else
			{
				alert(datos);
			}
			
	}); 

//////

 $(document).on('click', '#btn-enviar',function (){
	/* $("#grid-data").html('<i class="fa fa-spinner fa-spin fa-3x "></i>');
		$.post("../ftp/get_record_date",
		{
		fecha_inicial 	: $("#date-picker-1").val(),
		fecha_final		: $("#date-picker-2").val(),
		},
		
		function(data) 
		{
			$("#grid-data").html(data);
		});*/
		
		
		//////////////////////////////////////////////////////////
		
			var parametros = {
			"fecha_inicial" : $("#date-picker-1").val(),
			"fecha_final" :  $("#date-picker-2").val()
			};
		
			$.ajax({
			data:  parametros,
			url:   '../ftp/ftp/get_record_date',
			type:  'post',
		    beforeSend: function () {
				  $("#grid-data").html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x "></i></div');
			},
			success:  function (data) 
			{
				  $("#grid-data").html(data);
				  $('#prueba').dataTable({
					  
					   //CONVERTIMOS NUESTRO LISTADO DE LA FORMA DEL JQUERY.DATATABLES- PASAMOS EL ID DE LA TABLA
   "language": {
                "url": "../js/datatable/1.10.4/Spanish.json"
               },
	"lengthMenu": [[100, 200, 500, -1], [100, 200, 500, "All"]],
	'iDisplayLength': 100,
	"bSort": false,
	
	"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '.')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
			
			//montoBsSaren, Total over all pages
			  TotalmontoBsSaren = api
                .column( 12 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } );
				
			//montoBsSaren, Total over this page
            PagemontoBsSaren = api
                .column( 12, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			
			
			//montoBsSat, Total over all pages
			  TotalmontoBsSat = api
                .column( 14 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } );
				
			 // montoBsSat, Total over this page
            PagemontoBsSat = api
                .column( 14, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			
			
            // total tramite, Total over all pages
            total_tramite = api
                .column( 15 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } );
 
            // total tramtie, Total over this page
            pageTotalTramite = api
                .column( 15, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
			//
			//montoBsSaren
			sumTotalPagemontoBsSaren= parseFloat(PagemontoBsSaren).toFixed(2);
			sumTotalPagemontoBsSaren=sumTotalPagemontoBsSaren.replace('.',',');
			sumTotalmontoBsSaren= parseFloat(TotalmontoBsSaren).toFixed(2);
			sumTotalmontoBsSaren=sumTotalmontoBsSaren.replace('.',',');
			
			//montoBsSat
			sumTotalPagemontoBsSat= parseFloat(PagemontoBsSat).toFixed(2);
			sumTotalPagemontoBsSat=sumTotalPagemontoBsSat.replace('.',',');
			sumTotalmontoBsSat= parseFloat(TotalmontoBsSat).toFixed(2);
			sumTotalmontoBsSat=sumTotalmontoBsSat.replace('.',',');
			
			
			//total tramite
			sumTotalPageTramite= parseFloat(pageTotalTramite).toFixed(2);
			sumTotalPageTramite=sumTotalPageTramite.replace('.',',');
			sumTotalTramite= parseFloat(total_tramite).toFixed(2);
			sumTotalTramite=sumTotalTramite.replace('.',',');
			
			
        /*    $( api.column( 15 ).footer() ).html(
                'Total Pag. Bs. '+sumTotalPageTramite +' (Total: Bs. '+ sumTotalTramite +')'
            );*/
			$("#montoBsSaren").html('Total Pag. Bs. '+sumTotalPagemontoBsSaren +'<br> (Total: Bs. '+ sumTotalmontoBsSaren +')');
			$("#montoBsSat").html('Total Pag. Bs. '+sumTotalPagemontoBsSat +'<br> (Total: Bs. '+ sumTotalmontoBsSat +')');
			$("#total_tramite").html('Total Pag. Bs. '+sumTotalPageTramite +'<br> (Total: Bs. '+ sumTotalTramite +')');
        }
		
    
					  
					  
					  
					  });
			},
			error: function(xhr, status, error) 
			{
				alert("Inesperado "+status+": "+error);
			}
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
 });


$(".date-picker").datepicker();	


$(".date-picker").keypress(function(event) {event.preventDefault();});
});//fin jquery
