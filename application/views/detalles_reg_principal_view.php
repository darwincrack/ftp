<?php	echo anchor(base_url()."ftp/excel/".str_replace("/","_",$fecha_inicial).'/'.str_replace("/","_",$fecha_final),"Descargar <i class='fa fa-file-excel-o'></i>");?>

<table class="table table-hover table-striped" id="prueba">
    <thead>
    <tr>
            <th>FECHA</th>
            <th>Nº EXPEDIENTE</th>
            <th>Nº PUB</th>
            <th>C.I SOLICITANTE</th>
            <th>NOMBRE DEL SOLICITANTE</th>
            <th>TIPO DE ACTO</th>
            <th>BANCO</th>
            <th>TIPO OPERACION</th>
            <th>NUM OPERACION</th>
            <th>FECHA DE OPERACION</th>
            <th>UT VIGENTE</th>
            <th>CANTIDAD EN U.T DEL ACTO (SAREN)</th>
            <th>MONTO BS DEL ACTO (SAREN)</th>
            <th>CANTIDAD EN UT (SATDC)</th>
            <th>MONTO EN BS (SATDC)</th>
            <th>TOTAL TRAMITE</th>
            
    </tr>
     <tfoot>
            <tr>
            	<th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th id="montoUtSaren"></th>
				<th id="montoBsSaren"></th>
                <th id="montoUtSat"></th>
                <th id="montoBsSat"></th>
                <th id="total_tramite"></th>
               <!-- <th colspan="15" style="text-align:center"></th>
                <th id="prueee"></th> -->
            </tr>
        </tfoot>
    </thead>
 
    <tbody style="text-align:center;">
    <?php
	
	
	foreach($resultado as $fields){?>
		 <tr style="font-size:11px;">
            <td><?php  echo $fields['0']?></td>
            <td><?php  echo $fields['1']?></td>
            <td><?php  echo $fields['2']?></td>
            <td><?php  echo $fields['3']?></td>
            <td><?php  echo utf8_encode($fields['4'])?></td>
            <td><?php  echo utf8_encode($fields['5'])?></td>
            <td><?php  echo $fields['6']?></td>
            <td><?php  echo $fields['7']?></td>
            <td><?php  echo $fields[8]?></td>
            <td><?php  echo $fields['9']?></td>
            <td><?php  echo str_replace(".",",",number_format($fields[10], 2,",",""))?></td>
            <td><?php  echo str_replace(".",",",number_format($fields[11], 2,",",""))?></td>
            <td><?php  echo str_replace(".",",",number_format($fields[12], 2,",",""))?></td>
            <td><?php  echo str_replace(".",",",number_format($fields[13], 2,",",""))?></td>
            <td><?php  echo str_replace(".",",", number_format($fields[14], 2,",",""))?></td>
            <td><?php  echo str_replace(".",",", $fields[15])?></td>
            
         </tr>
	<?php }?>
    </tbody>
 
</table>