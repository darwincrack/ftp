<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Login</title>
<style>

.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}

</style>
<link rel="stylesheet" href="<?php echo  base_url()?>css/bootstrap.min.css">
</head>

<body>

<div class="container">
    <div class="row">
    	    
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Inicia Sesion para Entrar a Reporte SAREN</h1>
            <div class="account-wall">
                <img class="profile-img" src="<?php echo site_url('img/user.jpg') ?>" alt="">
                <?php 
				$attributes = array('class' => 'form-signin','autocomplete'=>'off');
				 echo form_open('login/user_login',$attributes); ?>
                 	<?php if($this->session->flashdata('usuario_incorrecto'))
							{
								echo "<p class='bg-danger'>".$this->session->flashdata('usuario_incorrecto')."</p>";
							}?>
                <?php echo form_error('usuario'); ?>
                <?php echo form_error('contrasena'); ?>
                <input type="text" name="usuario" class="form-control" placeholder="Nombre de Usuario" required autofocus>
                
                <input type="password" name="contrasena" class="form-control" placeholder="Contraseña" required>
                <button class="btn btn-lg btn-danger btn-block" type="submit">
                    Iniciar Sesion</button>
				<?php echo form_hidden('token',$token)?>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo  base_url()?>js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap Core JavaScript -->
	<script src="<?php echo  base_url()?>js/bootstrap.min.js"></script>
</body>
</html>