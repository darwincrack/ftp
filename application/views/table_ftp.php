<table class="table table-hover table-striped" id="lista_csv">
<thead>
  <tr>
  	<th>Fecha</th>
    <th>Sistema</th>
    <th>Ultima fecha de Modificacion</th>
    <th>Descargar</th>
    <th>Exportar a la Base de Datos</th>
  </tr>
</thead>
  <tbody style="text-align:center;">
<?php foreach ($list as $clave=>$valor):?>
 	   		<tr>
            <td data-order="<?php echo fecha_csv_mdy(basename($valor)) ?>"><?php echo anchor(base_url()."ftp/leerftp/".limpia_espacios(basename($valor))."",fecha_csv(basename($valor)))."</a><br>"; ?> </td>
            <td><?php echo anchor(base_url()."ftp/leerftp/".limpia_espacios(basename($valor))."",nombre_csv(basename($valor)))."</a><br>"; ?> </td>
            <td><?php echo date_format_full(date_file_modif($valor)) ?></td>
            <?php
			if (basename($valor)=="originaleshasta_20150206")
			{
				echo "<td><i class='fa fa-ban'></i></td>";
				echo "<td><i class='fa fa-ban'></i></td>";
			}
			else
			{
			
				echo "<td>".anchor(base_url()."ftp/descargar/".basename($valor),"<i class='fa fa-download'></i>",array('title' => basename($valor)))."</td>";
			
			echo "<td> <a style='cursor:pointer' title='".basename($valor)."' class='export_bd' id='".basename($valor)."'><i class='fa fa-cloud-upload'></i></a></td>";
			}
			 ?>
             
            </tr>
<?php  endforeach; ?>
</tbody>
</table>

