<div class="col-md-8 col-md-offset-3">
<form class="form-inline">
  <div class="form-group">
        <div class="controls">
            <div class="input-group">
                <input id="date-picker-1" type="text"  class="date-picker form-control"  placeholder="Fecha Inicial"  required/>
                <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
        </div>
 </div>
  <div class="form-group">
        <div class="controls">
            <div class="input-group">
                <input id="date-picker-2" type="text" class="date-picker form-control" placeholder="Fecha Final"  required/>
                <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
        </div>
 </div>
 
  <button type="button" class="btn btn-danger" id="btn-enviar">Enviar</button>
</form>
</div>
<br>
<br>
<br>
<div id="grid-data"></div>
