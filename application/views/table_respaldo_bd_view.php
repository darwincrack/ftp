
<table class="table table-hover table-striped" id="detalles_cvs">
    <thead>
    <tr>
            <th>FECHA</th>
            <th>Nº EXPEDIENTE</th>
            <th>Nº PUB</th>
            <th>C.I SOLICITANTE</th>
            <th>NOMBRE DEL SOLICITANTE</th>
            <th>TIPO DE ACTO</th>
            <th>BANCO</th>
            <th>TIPO OPERACION</th>
            <th>NUM OPERACION</th>
            <th>FECHA DE OPERACION</th>
            <th>UT VIGENTE</th>
            <th>CANTIDAD EN U.T DEL ACTO (SAREN)</th>
            <th>MONTO BS DEL ACTO (SAREN)</th>
            <th>CANTIDAD EN UT (SATDC)</th>
            <th>MONTO EN BS (SATDC)</th>
            <th>TOTAL TRAMITE</th>
            
    </tr>
     <tfoot>
            <tr>
            	<th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th id="montoUtSaren"></th>
				<th id="montoBsSaren"></th>
                <th id="montoUtSat"></th>
                <th id="montoBsSat"></th>
                <th id="total_tramite"></th>
               <!-- <th colspan="15" style="text-align:center"></th>
                <th id="prueee"></th> -->
            </tr>
        </tfoot>
    </thead>
 
    <tbody style="text-align:center;">
    <?php foreach ($item as $list_item):?>
		 <tr style="font-size:11px;">
            <td><?php  echo $list_item['fecha']?></td>
            <td><?php  echo $list_item['nro_expediente']?></td>
            <td><?php  echo $list_item['nro_pub']?></td>
            <td><?php  echo $list_item['ci_solicitante']?></td>
            <td><?php  echo $list_item['nombre_solicitante']?></td>
            <td><?php  echo $list_item['tipo_acto']?></td>
            <td><?php  echo $list_item['banco']?></td>
            <td><?php  echo $list_item['tipo_operacion']?></td>
            <td><?php  echo $list_item['num_operacion']?></td>
            <td><?php  echo $list_item['fecha_operacion']?></td>
            <td><?php  echo str_replace(".",",",$list_item['ut_vigente'])?></td>
            <td><?php  echo str_replace(".",",",$list_item['cant_ut_saren'])?></td>
            <td><?php  echo str_replace(".",",",$list_item['bs_saren'])?></td>
            <td><?php  echo str_replace(".",",",$list_item['cant_ut_sat'])?></td>
            <td><?php  echo str_replace(".",",",$list_item['bs_sat'])?></td>
            <td><?php  echo str_replace(".",",",$list_item['total_tratime'])?></td>
         </tr>
<?php endforeach;?>
    </tbody>
 
</table>

