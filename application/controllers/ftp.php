<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ftp extends CI_Controller {
	
	var $servidor	=	""; //nombre servidor ftp
	var $user		=	""; //usuario servidor ftp
	var $pass		=	""; //contraseña servidor ftp
	var $rutaPath	=	"";//ruta de ubicacion de los archivos csv en el servidor ftp
	var $mensaje	=	"";//texto de email
	var $filePath	=	"";//ruta completa del ftp


	function __construct()
    {
       parent::__construct();
		$this->load->library('ftp');
		$this->load->helper('funciones');
		$this->load->helper('download');
		$this->load->model('ftp_model'); 
		$this->load->helper('date');
		/*$this->servidor="ftp";
		$this->user="regprinc";
		$this->pass="54r3n.ftp";*/
		$this->servidor="192.168.0.99";
		$this->user="regprinc";
		$this->pass="54r3n.ftp";
;
		$this->rutaPath="/";
		$this->filePath = "ftp://$this->user:$this->pass@$this->servidor$this->rutaPath";
    }

	 function listaCsv()
	 {
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');

		$this->ftp->connect();
		$data['list'] = $this->ftp->list_files($this->rutaPath);
		$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
		$this->template->add_js('js//datatable/1.10.4/dataTables.bootstrap.js');
		$this->template->add_js('js/funciones_datatable.js');
		$this->template->add_js('js/funciones.js');
		$this->template->add_css('css/demo_table.css');
		
		$this->template->write('title', 'Exportar a la Base de datos', TRUE);
		$this->template->write_view('content', 'table_ftp',$data,TRUE);
		/*if($ruta==1)
		{
			$data['ruta']=$ruta;
			$this->template->write('title', 'Lista de Reportes', TRUE);
			$this->template->write_view('content', 'table_ftp',$data,TRUE);
		}
		else
		{
			$data['ruta']=$ruta;
			
		}
		*/

		$this->template->render();
		$this->ftp->close();
	}
	
	
	
	function leerftp($name_file)
	{
		
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
        if($this->_detectarextfile($name_file))
		{
			
			//$filePath = "ftp://$this->user:$this->pass@$this->servidor/$name_file";
			$data['valor']		=	$name_file;
		 	$data['csvData'] 	= 	$this->csvreader->parse_file($this->filePath.$name_file,FALSE);
			$nombre_csv="Reporte Sistema ".nombre_csv(basename($name_file))." Al ".fecha_csv(basename($name_file));
			$this->template->write('title', $nombre_csv, TRUE);
 		 	$this->template->write_view('content', 'csv_view',$data,TRUE);
			
			$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
			$this->template->add_js('js//datatable/1.10.4/dataTables.bootstrap.js');
			$this->template->add_js('js/funciones_datatable.js');
			$this->template->add_css('css/demo_table.css');
			
		}
		else
		{
			$this->template->write_view('content', 'no_encontrado', TRUE);
		}
		 $this->template->render();	
	}
	
	
	
	function _detectarextfile($name_file)
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		$trozos = explode(".", $name_file); 
		$extension = end($trozos); 
		// mostramos la extensión del archivo
		if($extension=="csv")
		{
			return true;
		}
			return false;
	}
	
	
	function descargar($name_file)
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		//$filePath = "ftp://bolivar:G3n35y5+ftp@192.168.0.99/Descargas/$name_file";
		$data = file_get_contents($this->filePath.$name_file); // Read the file's contents

		force_download($name_file, $data);
	}
	
	
	function _save_csv_bd($name_file)
	{
		
		$this->output->enable_profiler(TRUE);
		$item=$this->ftp_model->existecsv($name_file);
		
			if(empty($item))
			{ 
				$csvData  	= 	$this->csvreader->parse_file($this->filePath.$name_file,FALSE);
				$fecha_csv	=	fecha_csv_ymd($name_file);
				$msn		=	$this->ftp_model->set_csv($name_file,$fecha_csv,$csvData);
			}
			else
			{
				$msn	=	"Ya existe en la base de datos";
			}
		
		return $name_file.": ".$msn."<br>";
	}
	
	function mysqlup()
	{
		$this->ftp->connect();
		$list = $this->ftp->list_files($this->rutaPath);
		$this->ftp->close();
		foreach ($list as $clave=>$valor)
		{
		  if(basename($valor)!="originaleshasta_20150206")
		  {
		  	$this->mensaje .= $this->_save_csv_bd(basename($valor));
		  }
		}
		echo $this->mensaje;
		$this->_email($this->mensaje);

		
	}
	
	function email($mensaje)
	{
		$this->load->library('email');
		$this->email->from('satgdc@gdc.gob.ve', 'Sistema Monitoreo, FTP');
		$this->email->to('darwin.cedeno@gdc.gob.ve');
		//$this->email->cc('darwintherudito@gmail.com');
		$this->email->subject('Reporte FTP, '.date('Y-m-d H:i:s'));
		$this->email->message($mensaje);
		
		
		
		
		if(! $this->email->send())
		{
			$this->email->print_debugger();
		}
	

	}
	
	
	function overwrite_csv_bd()
	{
		 if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		 $validar_exist		=	$this->input->post('validar_exist');
		 $name_csv			=	$this->input->post('name_csv');
		 $ingresar_new		=	$this->input->post('ingresar_new');
		 $overwrite			=	$this->input->post('overwrite');
		
		//$filePath = "ftp://$this->user:$this->pass@$this->servidor/Descargas/$name_csv";
		
		if($validar_exist==1){
			$item=$this->ftp_model->existecsv($name_csv);
			
			if(empty($item))
			{
				//no existe en la bd
				echo "0";
			}
			else
			{
				echo "1";
			}
		}
		
		if($ingresar_new==1)
		{
			//$filePath 	= 	"ftp://$this->user:$this->pass@$this->servidor/Descargas/$name_csv";
			$csvData  	= 	$this->csvreader->parse_file($this->filePath.$name_csv,FALSE);
			$fecha_csv	=	fecha_csv_ymd($name_csv);
			$msn		=	$this->ftp_model->set_csv($name_csv,$fecha_csv,$csvData);
			echo $msn;
		}
		
		if($overwrite==1)
		{
			//$filePath 	= 	"ftp://$this->user:$this->pass@$this->servidor/Descargas/$name_csv";
			$csvData  	= 	$this->csvreader->parse_file($this->filePath.$name_csv,FALSE);
			$fecha_csv	=	fecha_csv_ymd($name_csv);
			$msn		=	$this->ftp_model->set_csv($name_csv,$fecha_csv,$csvData,TRUE);
			echo $msn;
		}
	}
	
	
	
	function list_backup_bd()
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		$data['item']=$this->ftp_model->get_csv();
		if(empty($data['item']))
		{ 
			$this->template->write_view('content', 'no_encontrado', TRUE);
		}
		else
		{
			$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
			$this->template->add_js('js//datatable/1.10.4/dataTables.bootstrap.js');
			$this->template->add_js('js/funciones_datatable.js');
			$this->template->add_css('css/demo_table.css');
			$this->template->write_view('content', 'table_respaldo_bd',$data,TRUE);
		}
		
		$this->template->write('title', 'Respaldo en la Base de Datos', TRUE);
		$this->template->render();
	}
	
	
	
	function detalles_report_bd($name_file)
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		$data['item']=$this->ftp_model->get_csv($name_file);
		
		if(empty($data['item']))
		{
			$this->template->write_view('content', 'no_encontrado', TRUE);
		}
		else
		{
			$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
			$this->template->add_js('js//datatable/1.10.4/dataTables.bootstrap.js');
			$this->template->add_js('js/funciones_datatable.js');
			$this->template->add_css('css/demo_table.css');
			
			$this->template->write_view('content', 'table_respaldo_bd_view',$data,TRUE);
		}


		$nombre_csv="Reporte Sistema ".nombre_csv($name_file)." Al ".fecha_csv($name_file);
		$this->template->write('title', $nombre_csv, TRUE);
		$this->template->render();	
	}
	
	
	function time_real()
	{
		
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		$this->template->add_css('css/jquery-ui-1.10.0.custom.css');
		$this->template->add_css('css/demo_table.css');
		$this->template->add_js('js/plugins/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/plugins/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
		$this->template->add_js('js//datatable/1.10.4/dataTables.bootstrap.js');
		$this->template->add_js('js/funciones.js');
		
		$this->template->write('title', "Registro Principal (Tiempo Real)", TRUE);
		$this->template->write_view('content', 'real_time_view',TRUE);
		$this->template->render();	
	}
	
	function get_record_date()
	{

	if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
	$this->template->add_js('js/funciones_datatable.js');
	$fecha_inicial	=	$this->input->post("fecha_inicial");
	$fecha_final	=	$this->input->post("fecha_final");
	$data['fecha_inicial']=$fecha_inicial;
	$data['fecha_final']=$fecha_final;
		
	try
	{
		
		//con servidor windows
		$conn = new PDO( "sqlsrv:server=registroppalccs.no-ip.info ; Database=REGISTRO", "usrsatdc", "U5r54tdc");
		
		//con servidor linux
		//$dsn = 'dblib:dbname=REGISTRO;host=registroppalccs.no-ip.info';
	   // $conn = new PDO($dsn,"usrsatdc","U5r54tdc");
		$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	}
	catch(Exception $e)
	{ 
		die( print_r( $e->getMessage() ) ); 
	}

	//$params = array($fecha_inicial, $fecha_final);
	$tsql = "EXECUTE	rep_solicitudes_por_fecha_fisco_todas
		@fecha1 = '$fecha_inicial',
		@fecha2 = '$fecha_final'";

	$stmt = $conn->prepare($tsql);
	$stmt->execute();
	$stmt->nextRowset();
	$result = $stmt->fetchAll(PDO::FETCH_BOTH);
	$data['resultado']=$result;
	$resultCount = count($result);
	if($resultCount > 0)
	{
		$this->load->view('detalles_reg_principal_view', $data);
		
	/*	foreach( $result as $row )
		{
			echo  number_format($row[14], 2).'         '.$row[14].'<BR>' ;
			
			
		}*/
	}
	else
	{
		$this->load->view('no_encontrado');
	}

		

	}
	
	function excel($fecha_inicial,$fecha_final)
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		$fecha_inicial	=	str_replace("_","/",$fecha_inicial);
		$fecha_final	=	str_replace("_","/",$fecha_final);
		
		try
		{
			//con servidor windows
			$conn = new PDO( "sqlsrv:server=registroppalccs.no-ip.info ; Database=REGISTRO", "usrsatdc", "U5r54tdc");
			//con servidor linux
			//$dsn = 'dblib:dbname=REGISTRO;host=registroppalccs.no-ip.info';
			//$conn = new PDO($dsn,"usrsatdc","U5r54tdc");
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}
		catch(Exception $e)
		{ 
			die( print_r( $e->getMessage() ) ); 
		}
	
		//$params = array($fecha_inicial, $fecha_final);
		$tsql = "EXECUTE	rep_solicitudes_por_fecha_fisco_todas
		@fecha1 = '$fecha_inicial',
		@fecha2 = '$fecha_final'";
	
		$stmt = $conn->prepare($tsql);
		//$stmt->execute($params);
		$stmt->execute();
		// solo es necesario con servidor windows ($stmt->nextRowset();)
		$stmt->nextRowset();
		$result = $stmt->fetchAll(PDO::FETCH_BOTH);
		//$data['resultado']=$result;
		$resultCount = count($result);
		if($resultCount > 0)
		{
			//load our new PHPExcel library
			$this->load->library('excel');
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('test worksheet');
			//titulos
			$this->excel->getActiveSheet()->SetCellValue("A1",'FECHA');
			$this->excel->getActiveSheet()->SetCellValue("B1",'Nº EXPEDIENTE');
			$this->excel->getActiveSheet()->SetCellValue("C1",'Nº PUB');
			$this->excel->getActiveSheet()->SetCellValue("D1",'C.I SOLICITANTE');
			$this->excel->getActiveSheet()->SetCellValue("E1",'NOMBRE DEL SOLICITANTE');
			$this->excel->getActiveSheet()->SetCellValue("F1",'TIPO DE ACTO');
			$this->excel->getActiveSheet()->SetCellValue("G1",'BANCO');
			$this->excel->getActiveSheet()->SetCellValue("H1",'TIPO OPERACION');
			$this->excel->getActiveSheet()->SetCellValue("I1",'NUM OPERACION');
			$this->excel->getActiveSheet()->SetCellValue("J1",'FECHA DE OPERACION');
			$this->excel->getActiveSheet()->SetCellValue("K1",'UT VIGENTE');
			$this->excel->getActiveSheet()->SetCellValue("L1",'CANTIDAD EN U.T DEL ACTO (SAREN)');
			$this->excel->getActiveSheet()->SetCellValue("M1",'MONTO BS DEL ACTO (SAREN)');
			$this->excel->getActiveSheet()->SetCellValue("N1",'CANTIDAD EN UT (SATDC)');
			$this->excel->getActiveSheet()->SetCellValue("O1",'MONTO EN BS (SATDC)');
			$this->excel->getActiveSheet()->SetCellValue("P1",'TOTAL TRAMITE');
			
				//ajustamos el ancho de las columnas al ancho del texto
			$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
			$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
			
			$i=2;
			foreach( $result as $row )
			{
				
				$this->excel->getActiveSheet()->SetCellValue("A".$i, $row['0']);
				$this->excel->getActiveSheet()->SetCellValue("B".$i, $row['1']);
				$this->excel->getActiveSheet()->SetCellValue("C".$i, $row['2']);
				$this->excel->getActiveSheet()->SetCellValue("D".$i, $row['3']);
				$this->excel->getActiveSheet()->SetCellValue("E".$i, $row['4']);
				$this->excel->getActiveSheet()->SetCellValue("F".$i, $row['5']);
				$this->excel->getActiveSheet()->SetCellValue("G".$i, $row['6']);
				$this->excel->getActiveSheet()->SetCellValue("H".$i, $row['7']);
				$this->excel->getActiveSheet()->SetCellValue("I".$i, $row['8']);
				$this->excel->getActiveSheet()->SetCellValue("J".$i, $row['9']);
				$this->excel->getActiveSheet()->SetCellValue("K".$i, $row['10']);
				$this->excel->getActiveSheet()->SetCellValue("L".$i, $row['11']);
				$this->excel->getActiveSheet()->SetCellValue("M".$i, $row['12']);
				$this->excel->getActiveSheet()->SetCellValue("N".$i, $row['13']);
				$this->excel->getActiveSheet()->SetCellValue("O".$i, $row['14']);
				$this->excel->getActiveSheet()->SetCellValue("P".$i, $row['15']);
				$i++;
			}
			
		 //set aligment to center for that merged cell (A1 to D1)
		  $this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		  
		   
		  $filename='registro_principal.xlsx'; //save our workbook as this file name
		  header('Content-Type: application/vnd.ms-excel'); //mime type
		  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		  header('Cache-Control: max-age=0'); //no cache
					   
		  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		  //if you want to save it as .XLSX Excel 2007 format
		  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
		  //force user to download the Excel file without writing it to server's HD
		  $objWriter->save('php://output');
		}
		else
		{
			$this->load->view('no_encontrado');
		}
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}
	
	
}