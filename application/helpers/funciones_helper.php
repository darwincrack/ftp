<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2015, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
 
 /**
 * Retorna la ip real de usuario
 */
 
  function getRealIP() {
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
		
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	
	return $_SERVER['REMOTE_ADDR'];
}

  /**
 * desencripta una cadena de caracteres
 *@string Es la cadena a encryptar
 *@key Es la llave para encryptar 
 */
  function encrypt($string, $key='c0rr35p0nd3nc14+') {
	 $result = '';
	 for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)+ord($keychar));
		$result.=$char;
	 }
	 return base64_encode($result);
  }
  
 /**
 * desencripta una cadena de caracteres
 *@string Es la cadena a desencryptar
 *@key Es la llave para desencryptar 
 */
  function decrypt($string, $key='c0rr35p0nd3nc14+') {
	 $result = '';
	 $string = base64_decode($string);
	 for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)-ord($keychar));
		$result.=$char;
	 }
	 return $result;
  }
  
  
  
/**
 * limpia todos los espacios en blanco de una cadena
 *@cadena Es la cadena a quitar espacios en blanco

 */
  
  function limpia_espacios($cadena){

    $cadena = str_replace(' ', '', $cadena);
    return $cadena;
}



/**
 * extrae la fecha del nombre del archivo .csv y la convierte en un formato entendible DD/MM/YYYY
 *@name_csv es el nombre del archivo .csv

 */

	function fecha_csv($name_csv)
	{
		$fecha = end(explode("_", $name_csv));
		$fecha= str_replace(".csv","",$fecha);
		$fecha= str_replace(" ","",$fecha);
		$anho = substr($fecha, 0, 4);
		$mes = substr($fecha, 4, 2);
		$dia = substr($fecha, 6, 8);
		$fecha=$dia."/".$mes."/".$anho;
		return $fecha;
	}
	

/**
 * extrae la fecha del nombre del archivo .csv y la convierte en un formato entendible MM/DD/YYYY
 *@name_csv es el nombre del archivo .csv

 */

	function fecha_csv_mdy($name_csv)
	{
		$fecha = end(explode("_", $name_csv));
		$fecha= str_replace(".csv","",$fecha);
		$fecha= str_replace(" ","",$fecha);
		$anho = substr($fecha, 0, 4);
		$mes = substr($fecha, 4, 2);
		$dia = substr($fecha, 6, 8);
		$fecha=$mes."/".$dia."/".$anho;
		return $fecha;
	}
	
	
		function fecha_csv_ymd($name_csv)
	{
		$fecha = end(explode("_", $name_csv));
		$fecha= str_replace(".csv","",$fecha);
		$fecha= str_replace(" ","",$fecha);
		$anho = substr($fecha, 0, 4);
		$mes = substr($fecha, 4, 2);
		$dia = substr($fecha, 6, 8);
		$fecha=$anho."/".$mes."/".$dia;
		return $fecha;
	}
	
	
	
	/**
 	* atraves de la nomenclatura del nombre del archivo se determina de que sistema proviene
 	*@name_csv es el nombre del archivo
 	*/
	function nombre_csv($name_csv)
	{
		$nombre_csv=substr($name_csv, 0, 10);
		
		if($nombre_csv=="regprinc01")
		{
			$nombre_csv="Actual";
		}
		else if ($nombre_csv=="regprinc02")
		{
			$nombre_csv="Piloto";
		}
		else
		{
			$nombre_csv="Sistema Desconocido, (".$name_csv.")";
		}
		return $nombre_csv;
	}
	
	//recibe la ruta del archivo y devuelve la ultima fecha de modificacion del mismo
	
	function date_file_modif($name_file)
	{
		$user="regprinc";
		$pass="54r3n.ftp";
		$servidor="192.168.0.99";
		
		$filePath = "ftp://$user:$pass@$servidor/$name_file";
		return filemtime($filePath);
	}
	
	
	function date_format_full($date)
	{
		return date ("d-m-Y h:i:s A.",$date);
	}
	
	function date_format_mdy($date)
	{
		return date ("m/d/Y",$date);
	}
	
	
		