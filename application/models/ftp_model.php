<?php class Ftp_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	function existecsv($name_csv)
	{
		$this->db->select('satdc_csv.satdc_nombre_csv.nombre');
		$this->db->from('satdc_csv.satdc_nombre_csv');
		$this->db->where('satdc_csv.satdc_nombre_csv.nombre', $name_csv);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function set_csv($name_file,$fecha_csv,$csvData,$overwrite=FALSE)
	{
		
		$this->db->trans_begin();
		if($overwrite==TRUE)
		{
			
			$this->db->select('satdc_csv.satdc_nombre_csv.id');
			$this->db->from('satdc_csv.satdc_nombre_csv');
			$this->db->where('satdc_csv.satdc_nombre_csv.nombre', $name_file);
			$query = $this->db->get();
			$item=$query->row();
			
			$this->db->where('id_nombre_csv', $item->id);
			$this->db->delete('satdc_csv.satdc_detalles_csv');
			
			$this->db->where('id', $item->id);
			$this->db->delete('satdc_csv.satdc_nombre_csv');
		}
		
		
		
		//insertar_unidad
			$data = array(
			'nombre' => $name_file,
			'fecha'	 => $fecha_csv,
			'fecha_registro'=>date('Y-m-d H:i:s')
			);
			$this->db->insert('satdc_csv.satdc_nombre_csv', $data); 
			$id_csv=$this->db->insert_id();
		
		
		foreach($csvData as $id=>$fields)
		{
		  //satdc_detalles_csv
		  $data = array(
			'id_nombre_csv' 	 => $id_csv ,
			'fecha' 			 => $fields['0'],
			'nro_expediente'	 => $fields['1'],
			'nro_pub'	 		 => $fields['2'],
			'ci_solicitante'	 => $fields['3'],
			'nombre_solicitante' => utf8_encode($fields['4']),
			'tipo_acto'	 		 => utf8_encode($fields['5']),
			'banco'	 			 => $fields['6'],
			'tipo_operacion'	 => $fields['7'],
			'num_operacion'	  	 => $fields['8'],
			'fecha_operacion'	 => $fields['9'],
			'ut_vigente'	 	 => $fields['10'],
			'cant_ut_saren'	 	 => $fields['11'],
			'bs_saren'	 		 => $fields['12'],
			'cant_ut_sat'	 	 => $fields['13'],
			'bs_sat'	 		 => $fields['14'],
			'total_tratime'	 	 => $fields['15'],
		  );
		  $this->db->insert('satdc_csv.satdc_detalles_csv', $data); 
		}
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return "finalizo con error, rollback";
		}
		else
		{
			$this->db->trans_commit();
			$this->db->close();
			return "finalizado con exito, commit";
		}
	}
	
	
	function get_csv($name_csv=FALSE)
	{
		if($name_csv===FALSE){
		
			$this->db->select(" satdc_nombre_csv.id,satdc_nombre_csv.nombre,DATE_FORMAT(satdc_nombre_csv.fecha,('%d/%m/%Y')) as fecha");
			$this->db->from('satdc_csv.satdc_nombre_csv');
			$query = $this->db->get();
			return $query->result_array();
		}
		else
		{
			$this->db->select("satdc_detalles_csv.fecha,
							  satdc_detalles_csv.nro_expediente,
							  satdc_detalles_csv.nro_pub,
							  satdc_detalles_csv.ci_solicitante,
							  satdc_detalles_csv.nombre_solicitante,
							  satdc_detalles_csv.tipo_acto,
							  satdc_detalles_csv.banco,
							  satdc_detalles_csv.tipo_operacion,
							  satdc_detalles_csv.num_operacion,
							  satdc_detalles_csv.fecha_operacion,
							  satdc_detalles_csv.ut_vigente,
							  satdc_detalles_csv.cant_ut_saren,
							  satdc_detalles_csv.bs_saren,
							  satdc_detalles_csv.cant_ut_sat,
							  satdc_detalles_csv.bs_sat,
							  satdc_detalles_csv.total_tratime");
			$this->db->from('satdc_csv.satdc_detalles_csv');
			$this->db->join('satdc_csv.satdc_nombre_csv', 'satdc_csv.satdc_detalles_csv.id_nombre_csv = satdc_csv.satdc_nombre_csv.id');
			$this->db->where('satdc_csv.satdc_nombre_csv.nombre', $name_csv);
			$query = $this->db->get();
			return $query->result_array();
		}
		
	}

}