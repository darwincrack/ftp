<?php class Control_usuarios_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}


	public function get_usuarios($name_user)
	{
		//$nombre_user=$this->db->escape($name_user);
		/*$this->db->select('usuario.login,funcionario.nombre1,funcionario.apellido1,inventario_timbres.unidad.id_unidad,usuario.usuario_id');
		$this->db->from('usuario');
		$this->db->join('funcionario', 'usuario.funcionario_id = funcionario.funcionario_id');
		$this->db->join('usuario_unidad', 'usuario.usuario_id = usuario_unidad.usuario_id','left');
		$this->db->join('inventario_timbres.unidad', 'usuario_unidad.id_unidad = inventario_timbres.unidad.id_unidad','left');
		$this->db->where('funcionario.activo',1);
		$this->db->like('usuario.login', $name_user);
		$this->db->or_like('funcionario.nombre1', $name_user);
		$this->db->or_like('inventario_timbres.unidad.descripcion', $name_user);
		$this->db->order_by("inventario_timbres.unidad.id_unidad", "desc");
		$query = $this->db->get();
		return $query->result_array();*/
		
		
	  $sql = "SELECT usuario.login
	  , funcionario.nombre1
	  , funcionario.apellido1
	  , inventario_timbres.unidad.id_unidad
	  , usuario.usuario_id
	  ,funcionario.activo
	  FROM (usuario)
	  JOIN funcionario ON usuario.funcionario_id = funcionario.funcionario_id
	  LEFT JOIN usuario_unidad ON usuario.usuario_id = usuario_unidad.usuario_id
	  LEFT JOIN inventario_timbres.unidad ON usuario_unidad.id_unidad = inventario_timbres.unidad.id_unidad
	  WHERE (funcionario.activo =  1)
	  AND  (usuario.login  LIKE '%$name_user%'
	  OR  funcionario.nombre1  LIKE '%$name_user%'
	  OR  inventario_timbres.unidad.descripcion  LIKE '%$name_user%')
	  ORDER BY inventario_timbres.unidad.id_unidad desc ";
	  $query=	$this->db->query($sql, array($name_user));
	  return $query->result_array();
	}

	public function get_descripcion_areas()
	{

		$this->db->select('inventario_timbres.unidad.descripcion, inventario_timbres.unidad.id_unidad');
		$this->db->from('inventario_timbres.unidad');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_ubicacion($id_user,$id_ubicacion)
	{
		//ubicacion en el sistema de verificador
		if($id_ubicacion==5)
		{
			$perfil_tipo_acto_id=2; 
		}
		else
		{
			$perfil_tipo_acto_id=1;
		}
		/////
		$this->db->select('usuario_id');
		$this->db->from('usuario_unidad');
		$this->db->where('usuario_id', $id_user); 
		$query = $this->db->get();
		
		if($query->num_rows()>=1)
		{
			
			//actualizar unidad
			$data = array(
			'id_unidad' => $id_ubicacion
			);
			$this->db->where('usuario_id', $id_user);
			$this->db->update('usuario_unidad', $data);	
			
			//actualizar tipo de acto id en verificador
			$data = array(
			'perfil_tipo_acto_id' => $perfil_tipo_acto_id
			);
			$this->db->where('usuario_id', $id_user);
			$this->db->update('usuario', $data);	
			
			
			
		}
		else
		{	//insertar_unidad
			$data = array(
			'usuario_id' => $id_user ,
			'id_unidad'	 => $id_ubicacion 
			);
			$this->db->insert('usuario_unidad', $data); 
		}
	}


	public function reset_password($usuario_id,$pass)
	{
		
			$data = array(
			'clave' => md5($pass),

			);
			$this->db->where('usuario_id', $usuario_id);
			$this->db->update('usuario', $data);
			return $this->db->affected_rows();	
	}

	
	
}