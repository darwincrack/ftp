<?php class Login_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	
	function get_login_user()
	{
		$user_name=$this->input->post('usuario');
		$pass_user=md5($this->input->post('contrasena'));
		
		$this->db->select('usuario.login,funcionario.area_id');
		$this->db->from('usuario');
		$this->db->join('funcionario', 'usuario.funcionario_id = funcionario.funcionario_id');
		$this->db->where('funcionario.activo',1);
		$this->db->where('usuario.login',$user_name);
		$this->db->where('usuario.clave',$pass_user);
		$this->db->where('usuario.nivel_verif',1);
		$query = $this->db->get();
		
		if($query->num_rows() == 1)
		{
			return $query->row();
		}else{
			$this->session->set_flashdata('usuario_incorrecto','Usuario y/o Contraseña Invalidos');
			redirect(base_url().'login','refresh');
		}
				
				
	}
}